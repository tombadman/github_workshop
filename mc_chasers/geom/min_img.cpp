#include <vector>
#include <cmath>
#include "mc_util.hpp"


std::vector<int> min_img(
                         std::vector<int> ri,
                         std::vector<int> rj,
                         int              d_cube_dim,
                         int              d_grid_points
                         )
{
    std::vector<int> rij;
    
    for(int j = 0; j < d_cube_dim; ++j)
    {
        // =============
        //
        // find the min image between walkers i and j.
        //
        // =============
        
        int rij_ = ri[j] - rj[j];
        
        
        if(rij_ > std::floor((d_grid_points/2)))
        {
            rij_ -= d_grid_points;
        }
        
        if(rij_ <= -(std::floor(d_grid_points/2)))
        {
            rij_ += d_grid_points;
        }
        
        rij.push_back(rij_);
        
        // =============
    }
    
    return rij;
}