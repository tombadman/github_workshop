#include <vector>

std::vector<int> wrap_coord(
                            const std::vector<int> coord,
                            const int              dim,
                            const int              d_grid_points
                            )


{
    std::vector<int> coord_ = coord;
    
    for(int i = 0; i < dim; ++i)
    {
        if(coord_[i] > d_grid_points)
        {
            coord_[i] -= d_grid_points;
        }
        
        if(coord_[i] <= 0)
        {
            coord_[i] += d_grid_points;
        }
    }
    
    coord_.push_back(coord[dim+1]); // don't forget to include the mover type
    
    return coord_;
}