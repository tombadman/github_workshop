#ifndef MC_UTIL_HPP
#define MC_UTIL_HPP

#include <vector>
#include <tuple>

std::tuple<
           int,                             // nwalkers
           std::vector<std::vector<std::vector<int>>>
           > setup_walkers(
                           int       nwalkers, // number of walkers (may be overridden)
                           const int d_cube_dim,
                           const int d_grid_points,
                           const int nchasers,
                           const int ntargets
                           );

std::vector<int> take_step(
                           const std::vector<int>              rij,
                           const double                        prob,
                           const int                           dim,
                           const int                           L,
                           const int                           type
                           );

int find_neighbor(
                  int idx,
                  int dim,
                  int grid_pts,
                  std::vector<std::vector<int>> movers
                  );


void output_chase_cubes(
                        int                                              d_cube_dim,
                        const int                                        d_grid_points,
                        const int                                        total,
                        const int                                        nsteps,
                        const std::vector<std::vector<std::vector<std::vector<int>>>> store_movers
                        );
#endif