/*
 Copyright 2014-Present Algorithms in Motion LLC
 
 This file is part of jScience.
 
 jScience is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 jScience is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with jScience.  If not, see <http://www.gnu.org/licenses/>.
*/
// CREATED    : <8/2/2013
// LAST UPDATE:  11/21/2013

//#include "jrandnum.hpp"

// STL
#include <algorithm>  // std::generate_n
#include <array>      // std::array
#include <functional> // std::ref
#include <numeric>    // std::iota
#include <random>     // random number stuff
#include <utility>    // std::swap
#include <vector>     // std::vector


static std::mt19937 g_rng;
static bool g_rng_seeded = false;


//========================================================================
//========================================================================
//
// NAME: void seed_Mersenne_twister()
//
// DESC: Seeds the Mersenne twiser PRNG.
//
// NOTES:
//     ! << JMM >> :: should allow a real random number from random.org to be passed here
//
//========================================================================
//========================================================================
void seed_Mersenne_twister()
{
    const int RNG_NBURN = 99999;
    
    //*********************************************************
    
    // GENERATE A SEED SEQUENCE WITH ENOUGH RANOM DATA TO FILL THE std:mt19937 STATE
    std::array<int, std::mt19937::state_size> seed_data;
    std::random_device rd;
    std::generate_n( seed_data.data(), seed_data.size(), std::ref(rd) );
    std::seed_seq seq(std::begin(seed_data), std::end(seed_data));
    
    // SEED THE RNG
    g_rng = std::mt19937(seq);
    
    // BURN A NUMBER OF VALUES
    std::normal_distribution<double> normal_dist(0.0, 1.0);
    for(int i = 0; i < RNG_NBURN; ++i)
    {
        normal_dist(g_rng);
    }
    
    g_rng_seeded = true;
}


//========================================================================
//========================================================================
//
// NAME: double rand_num_uniform_Mersenne_twister(double a, double b)
//       int rand_num_uniform_Mersenne_twister(int a, int b)
//
// DESC: Produces a (pseudo) random number, distributed uniformly on the interval [a, b].
//
// NOTES:
//     ! These routines cannot be (easily or efficiently) templated, because they call different STL functions depending on type. 
//
//========================================================================
//========================================================================
double rand_num_uniform_Mersenne_twister(double a, double b)
{
    if( !g_rng_seeded )
    {
        seed_Mersenne_twister();
    }
    
    std::uniform_real_distribution<> dis(a, b);

    return dis(g_rng);
}

int rand_num_uniform_Mersenne_twister(int a, int b)
{
    if( !g_rng_seeded )
    {
        seed_Mersenne_twister();
    }
    
    std::uniform_int_distribution<> dis(a, b);
    
    return dis(g_rng);
}


//========================================================================
//========================================================================
//
// NAME: double rand_num_normal_Mersenne_twister(double mean, double stddev)
//
// DESC: Produces a (pseudo) random number, normally distributed.
//
//========================================================================
//========================================================================
double rand_num_normal_Mersenne_twister(double mean, double stddev)
{
    if( !g_rng_seeded )
    {
        seed_Mersenne_twister();
    }
    
    std::normal_distribution<double> normal_dist(mean, stddev);
    
    return normal_dist(g_rng);
}


//========================================================================
//========================================================================
//
// NAME: void n_unique_rand_ints(int n, int min, int max, std::vector<int> &ints)
//
// DESC: Generates n random uniqe ints between min and max according to the Knuth--Fisher--Yates algorithm, storing the results in ints.
//
// NOTES:
//     ! << JMM >> :: COULD PROBABLY BE USED IN A MORE GENERAL WAY TO SHUFLLE
//
//========================================================================
//========================================================================
void n_unique_rand_ints(int n, int min, int max, std::vector<int> &ints)
{
    ints.clear();
    
    std::vector<int> idx( (max - min + 1) );
    std::iota( idx.begin(), idx.end(), min );
    
    int maxr = idx.size() - 1;
    
    for( int i = 0; i < n; ++i )
    {
        int r = rand_num_uniform_Mersenne_twister( 0, maxr );
        
        std::swap( idx[r], idx[maxr] );

        --maxr;
    }
    
    for( int i = 0; i < n; ++i )
    {
        ints.push_back( idx[maxr+1 + i] );
    } 
}
