// STL
#include <vector>
#include <string>
#include <fstream>
#include <iostream>     // std::cout
#include <algorithm>    // std::random_shuffle
#include <vector>       // std::vector
#include <ctime>        // std::time
#include <cstdlib>      // std::rand, std::srand

#ifdef PARALLEL
// MPI
#include <mpi.h>
#endif

#include "mc_util.hpp"
#include "jrandnum.hpp"
#include "geom.hpp"
#include "cubes.hpp"

int main(int argc, char* argv[])
{
   
    
#ifdef PARALLEL
    // NOTE: MPI_Init() should be the first thing called in an MPI program ...
    MPI_Init(
             &argc,
             &argv
             );
    
    int mpi_nprocessors;
    MPI_Comm_size(
                  MPI_COMM_WORLD,
                  &mpi_nprocessors
                  );
    
    int mpi_my_id;
    MPI_Comm_rank(
                  MPI_COMM_WORLD,
                  &mpi_my_id
                  );
    
    const int mpi_master_id = 0;
#endif

    
    
    int    nwalkers       = std::stoi(argv[1]);
    int    nsteps         = std::stoi(argv[2]);
   // int    d_cube_dim     = std::stoi(argv[3]);
    int    d_grid_points  = std::stoi(argv[3]);
    int    nchasers       = std::stoi(argv[4]);
    int    ntargets       = std::stoi(argv[5]);
    double prob           = std::stoi(argv[6]); // this turns the probability of a chaser being lazy; [0,1].
    
    int d_cube_dim = 3;
    
    
    // the direct chaser samples towards the chased. probabilities change to 1/3, 1/3 in 3d.
    //
    //
    //     (direct)
    //       o-->    1/2 pr
    //       |
    //       v
    //
    //       1/2 pr        (chased)
    //                       o--> 1/2 pr
    //                       |
    //                       v
    //                      1/2 pr
    //
    //
    //
    
    
    // the lazy chaser sames towards the chased. probabilities change to 1/8 and 1/3 in 3d.
    //
    //        1/4 pr
    //        ^       (lazy)
    // 1/4 pr |
    //     <--o-->  1/4 pr
    //        |
    //        v
    //
    //        1/4 pr        (chased)
    //                       o--> 1/2 pr
    //                       |
    //                       v
    //                      1/2 pr
    //
    //
    //
    
    
   // first index walker [walker #][coords, mover type]
    
    // initialize coordinates randomly.
    
    auto get_walkers = setup_walkers(
                                     nwalkers,       // number of walkers (may be overridden)
                                     d_cube_dim,     // dimensionality of the 'arena'
                                     d_grid_points,  // grid points
                                     nchasers,
                                     ntargets
                                     );
    
    
    
    nwalkers = std::get<0>(get_walkers);
    auto movers   = std::get<1>(get_walkers);
    
    int nwalkers_total = nwalkers;
    
#ifdef PARALLEL
    if(mpi_my_id != mpi_master_id)
    {
        MPI_Send(&nwalkers, 1, MPI_INT, mpi_master_id, 0, MPI_COMM_WORLD);
    }
    else
    {
        for(int i = 0; i < mpi_nprocessors; ++i)
        {
            if(i != mpi_master_id)
            {
                int nwalkersi;
                MPI_Recv(&nwalkersi, 1, MPI_INT, i, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
                
                nwalkers += nwalkersi;
            }
        }
    }
#endif
    
    // [step #][walker #][coords, ID]
    std::vector<std::vector<std::vector<std::vector<int>>>> store_mover_data;
    
    int flag = 1;
    int success_nsteps;
    int which_walker;
    
    
    
    // begin the chasing
    
    for(int i = 0; i < nsteps; ++i)
    {
        if(flag == 0)
        {
            break;
        }
        
        for(int W = 0; W < nwalkers; ++W)
        {
            
            int rand = rand_num_uniform_Mersenne_twister(0,movers[W].size());
            
            //first, randomize the order of the movers.
            //movers[W] =
            
            std::random_shuffle (movers[W].begin(), movers[W].end());//, rand);
            
            //std::cout << " problem?" << '\n';
            
            //then, move them one by one.
            for(int p = 0; p < movers[W].size(); ++p)
            {
                
                std::cout << movers[W][p][0] << "  " << movers[W][p][1] << "   " << movers[W][p][2] << "   " << movers[W][p][3] <<'\n';
                // ========================== //
                //                            //
                //   update the coordinates   //
                //   of mover i in walker W   //
                //                            //
                // ========================== //
                
                // update all coordinates in this loop.
                // while updating coordinates, we must
                // make sure that no chasers are landing
                // on other chasers.
                //
                // meanwhile, we also must report back
                // if a target was caught. we should get all this
                // from take_step.
                
                //step 1: find neighbor. return rij.
                int idx_of_neighbor = find_neighbor(
                                                    p,
                                                    d_cube_dim,
                                                    d_grid_points,
                                                    movers[W]
                                                    );
                
             //   std::cout << "neighbor of " << p  << " is " << idx_of_neighbor << '\n';
                
                auto rij = min_img(
                                   movers[W][p],
                                   movers[W][idx_of_neighbor],
                                   d_cube_dim,
                                   d_grid_points
                                   );

                // step 2: take a step, and make sure
                // chasers aren't stepping on chasers
                // step 3: wrap the coordinate. make sure
                // the thing isn't out of bounds.
                
                int stepped_on_someone = 0;
                
                if(movers[W][p][d_cube_dim] == 1)
                {
                    while(stepped_on_someone == 0)
                    {
                        auto move_info = take_step(
                                                   rij,
                                                   prob,
                                                   d_cube_dim,
                                                   d_grid_points,
                                                   movers[W][p][d_cube_dim]
                                                   );
                        
                        movers[W][p][move_info[0]] += move_info[1];
                        
                        
                        movers[W][p] = wrap_coord(
                                                  movers[W][p],
                                                  d_cube_dim,
                                                  d_grid_points
                                                  );
                        
                        
                        
                        std::vector<int> match;
                        for(int q = 0; q < movers[W][q].size(); ++q)
                        {
                            int b = 0;
                            for(int d = 0; d < d_cube_dim; ++d)
                            {
                                if(movers[W][p][d] == 1 && p != q && movers[W][p][d_cube_dim] == movers[W][q][d_cube_dim])
                                {
                                    b += 1;
                                }
                            }
                            match.push_back(b);
                        }
                        
                        int q = *std::max_element(match.begin(), match.end());
                        
                        std::cout << q << '\n';
                        
                        if(q != d_cube_dim)
                        {
                            stepped_on_someone = 1;
                        }
                        
                    }
                    
                    if(movers[W][p][3] == 2)
                    {
                        auto move_info = take_step(
                                                   rij,
                                                   prob,
                                                   d_cube_dim,
                                                   d_grid_points,
                                                   movers[W][p][d_cube_dim]
                                                   );
                        
                        movers[W][p][move_info[0]] += move_info[1];
                        
                        
                        movers[W][p] = wrap_coord(
                                                  movers[W][p],
                                                  d_cube_dim,
                                                  d_grid_points
                                                  );
                    }

                
                }
                
                
                // step 4: see if a target was caught.
              /*
                int caught = -1;
                int idx_delete;
                for(int c = (movers[W].size()-1); c >= movers[W].size(); --c)
                {
                    int j = 0;
                    for(int d = 0; d < d_cube_dim; ++d)
                    {
                        if(movers[W][p][d] == movers[W][c][d] && p != c && movers[W][c][d_cube_dim] == 2)
                        {
                            ++j;
                        }
                    }
                    if(j == d_cube_dim)
                    {
                        std::cout << "chaser " << c << " got CAUGHT!" << '\n';
                        
                        movers[W][p].erase(movers[W][p].begin()+c);
                        
                        // this should really only happen once per loop.
                    }
                }
               */
                
            }
            
            // see if anything got caught
            
            int caught = -1;
            int idx_delete;
            
            for(int c = (movers[W].size()-1); c >= 0; --c)
            {
                for(int p = c; p >= 0; --p)
                {
                    int j = 0;
                    
                    for(int d = 0; d < d_cube_dim; ++d)
                    {
                        if(movers[W][p][d] == movers[W][c][d] && p != c && movers[W][p][d_cube_dim] == 2)
                        {
                            j+=1;
                        }
                    }
                   // std::cout << j << '\n';
                    if(j == d_cube_dim)
                    {
                        std::cout << "chaser " << c << " got CAUGHT!" << '\n';
                        
                        movers[W][p].erase(movers[W][p].begin()+c);
                        
                        // this should really only happen once per loop.
                    }
                }
            }

        }
        store_mover_data.push_back(movers);
    }
    

#ifdef PARALLEL
        if(mpi_my_id == mpi_master_id)
        {
#endif
            std::cout << "going to output now!" << '\n';
            
            
            if(d_cube_dim == 3)
            {
                output_chase_cubes(
                                   d_cube_dim,
                                   d_grid_points,
                                   nchasers+ntargets,
                                   nsteps,
                                   store_mover_data
                                   );
            }
            
#ifdef PARALLEL
            }
#endif

    
    
#ifdef PARALLEL
    // NOTE: ... and MPI_Finalize should be the last thing called
    MPI_Finalize();
#endif
    
    return 0;
}