#include <fstream>
#include <vector>
#include <iostream>
#include <string>

std::vector<std::vector<std::vector<int>>> cubes(
                                                    const int c,
                                                    const std::vector<int> vec
                                                    )

{
    std::vector<std::vector<std::vector<int>>> cubes_(
                                                      4,
                                                      std::vector<std::vector<int>>(
                                                                                    5,
                                                                                    std::vector<int>(
                                                                                                     3, 0
                                                                                                     )
                                                                                    )
                                                      );
    
    //http://www.gnuplotting.org/plotting-cubes/
    
    int b = 2;
    /*
     0 0 0
     0 0 1
     0 1 1
     0 1 0
     0 0 0
     */
    
    cubes_[0][0][0] = vec[0];
    cubes_[0][0][1] = vec[1];
    cubes_[0][0][2] = vec[2];

    cubes_[0][1][0] = vec[0];
    cubes_[0][1][1] = vec[1];
    cubes_[0][1][2] = vec[2] + b;
    
    cubes_[0][2][0] = vec[0];
    cubes_[0][2][1] = vec[1] + b;
    cubes_[0][2][2] = vec[2] + b;
    
    cubes_[0][3][0] = vec[0];
    cubes_[0][3][1] = vec[1] + b;
    cubes_[0][3][2] = vec[2];
    
    cubes_[0][4][0] = vec[0];
    cubes_[0][4][1] = vec[1];
    cubes_[0][4][2] = vec[2];
    
    /*
    1 0 0
    1 0 1
    1 1 1
    1 1 0
    1 0 0
    */
    
    cubes_[1][0][0] = vec[0] + b;
    cubes_[1][0][1] = vec[1];
    cubes_[1][0][2] = vec[2];
    
    cubes_[1][1][0] = vec[0] + b;
    cubes_[1][1][1] = vec[1];
    cubes_[1][1][2] = vec[2] + b;
    
    cubes_[1][2][0] = vec[0] + b;
    cubes_[1][2][1] = vec[1] + b;
    cubes_[1][2][2] = vec[2] + b;
    
    cubes_[1][3][0] = vec[0] + b;
    cubes_[1][3][1] = vec[1] + b;
    cubes_[1][3][2] = vec[2];
    
    cubes_[1][4][0] = vec[0] + b;
    cubes_[1][4][1] = vec[1];
    cubes_[1][4][2] = vec[2];
    
    /*
    0 0 0
    1 0 0
    1 1 0
    0 1 0
    0 0 0
    */
     
    cubes_[2][0][0] = vec[0];
    cubes_[2][0][1] = vec[1];
    cubes_[2][0][2] = vec[2];
    
    cubes_[2][1][0] = vec[0] + b;
    cubes_[2][1][1] = vec[1];
    cubes_[2][1][2] = vec[2];
    
    cubes_[2][2][0] = vec[0] + b;
    cubes_[2][2][1] = vec[1] + b;
    cubes_[2][2][2] = vec[2];
    
    cubes_[2][3][0] = vec[0];
    cubes_[2][3][1] = vec[1] + b;
    cubes_[2][3][2] = vec[2];
    
    cubes_[2][4][0] = vec[0];
    cubes_[2][4][1] = vec[1];
    cubes_[2][4][2] = vec[2];
    
    /*
    0 0 1
    1 0 1
    1 1 1
    0 1 1
    0 0 1
    */
     
    cubes_[3][0][0] = vec[0];
    cubes_[3][0][1] = vec[1];
    cubes_[3][0][2] = vec[2] + b;
    
    cubes_[3][1][0] = vec[0] + b;
    cubes_[3][1][1] = vec[1];
    cubes_[3][1][2] = vec[2] + b;
    
    cubes_[3][2][0] = vec[0] + b;
    cubes_[3][2][1] = vec[1] + b;
    cubes_[3][2][2] = vec[2] + b;
    
    cubes_[3][3][0] = vec[0];
    cubes_[3][3][1] = vec[1] + b;
    cubes_[3][3][2] = vec[2] + b;
    
    cubes_[3][4][0] = vec[0];
    cubes_[3][4][1] = vec[1];
    cubes_[3][4][2] = vec[2] + b;
    
    
    /*
    std::ofstream ofs;
    ofs.open("cube.fct", std::ios::out);
    
    ofs << "# gnuplot function to create a cube" << '\n';
    ofs << "# Usage: cube(x,y,z,c)" << '\n';
    ofs << "cube(x,y,z,c) = sprintf('<echo \"\ " << '\n';
    
    ofs << "%f %f %f %i\n\" << '\n';
    ofs << "%f %f %f %i\n\" << '\n';
    ofs << "%f %f %f %i\n\" << '\n';
    ofs << "%f %f %f %i\n\" << '\n';
    ofs << "%f %f %f %i\n\" << '\n';
    ofs << "\n\ " << '\n';
    ofs << "%f %f %f %i\n\" << '\n';
    ofs << "%f %f %f %i\n\" << '\n';
    ofs << "%f %f %f %i\n\" << '\n';
    ofs << "%f %f %f %i\n\" << '\n';
    ofs << "%f %f %f %i\n\" << '\n';
    ofs << "\n\ " << '\n';
    ofs << "%f %f %f %i\n\" << '\n';
    ofs << "%f %f %f %i\n\" << '\n';
    ofs << "%f %f %f %i\n\" << '\n';
    ofs << "%f %f %f %i\n\" << '\n';
    ofs << "%f %f %f %i\n\" << '\n';
    ofs << "\n\ " << '\n';
    ofs << "%f %f %f %i\n\" << '\n';
    ofs << "%f %f %f %i\n\" << '\n';
    ofs << "%f %f %f %i\n\" << '\n';
    ofs << "%f %f %f %i\n\" << '\n';
    ofs << "%f %f %f %i\n\" << '\n';
    ofs << "\n\ " << '\n';
    ofs << "" << '\n';
    ofs << "" << '\n';
    ofs << "" << '\n';
    */
    return cubes_;
}
