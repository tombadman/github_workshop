// STL
#include <functional> // std::function<>
#include <numeric>    // std::iota()
#include <stdexcept>  // std::runtime_error()
#include <string>     // std::string
#include <tuple>      // std::tuple<>, std::make_tuple()
#include <vector>     // std::vector<>
#include <cmath>      // std::floor()
#include <iostream>


#ifdef PARALLEL
// MPI
#include <mpi.h>
#endif

// jScience
#include "jrandnum.hpp"

//#include "mc_chase.hpp"

//
// TODO: Should allow to specify a master_id as an argument (below it is *assumed* to be 0)
//

std::tuple<
           int,                             // nwalkers
           std::vector<std::vector<std::vector<int>>>
           >  setup_walkers(
                            int       nwalkers, // number of walkers (may be overridden)
                            const int d_cube_dim,
                            const int d_grid_points,
                            const int nchasers,
                            const int ntargets
                            )

{
    
#ifdef PARALLEL
    int mpi_nprocessors;
    MPI_Comm_size(MPI_COMM_WORLD, &mpi_nprocessors);
    int mpi_my_id;
    MPI_Comm_rank(MPI_COMM_WORLD, &mpi_my_id);
#endif
    
    int guarantee = std::pow(d_grid_points, d_cube_dim);
    int total = nchasers + ntargets;
    
    if(guarantee < total)
    {
        std::cout << "you can't have more movers than grid points. please pick a larger grid or less movers." << '\n';
        
    }
    
    
#ifdef PARALLEL
    // NOTE: In parallel, we need at least one walker per processor.
    
    if(mpi_nprocessors > nwalkers)
    {
        throw std::runtime_error("error in main(): mpi_nprocessors > nwalkers");
    }
#endif
    
    //===================================
    // DETERMINE WALKER RESPONSIBILITY
    //===================================
    
    // TODO: Maybe this goes to its own subroutine / file.
    
    int nwalkers0 = nwalkers;
    
    int walker_begin;
    int walker_end;
    
#ifdef PARALLEL
    
    // NOTE: since there is ONLY parallelization over walkers, that is all the input that needs modification
   
    int nrem = nwalkers%mpi_nprocessors; // the number of walkers modulo number of processors, eg 8%5=3
    nwalkers /= mpi_nprocessors;         // nwalkers per processor.
    
    walker_begin = mpi_my_id*nwalkers;
    
    // TODO: NOTE: This algorithm depends on the mpi_master_id being at mpi_my_id == 0
    // TODO: ... should update so that it takes in p (master_id), and keeps the load on p light
    if(mpi_my_id != 0)
    {
        if(mpi_my_id > nrem)
        {
            walker_begin += nrem;
        }
        else
        {
            walker_begin += (mpi_my_id-1);
            
            ++nwalkers;
        }
    }
    
    walker_end = walker_begin + nwalkers - 1;
    
#else
    walker_begin = 0;
    walker_end   = (nwalkers0 - 1);
#endif
    
    
    //=== ========================== ===//
    //  DISTRIBUTE OR GENERATE WALKERS  //
    //=== ========================== ===//
    
    std::vector<std::vector<int>> movers;
    std::vector<std::vector<std::vector<int>>> all_movers;
    
    //this loop ensures unique coordinate for every mover.
    
    for(int w = 0; w < nwalkers; ++w)
    {
        for(int m = 0; m < total; ++m)
        {
            std::vector<int> mover(d_cube_dim);
            
            for(int i = 0; i < d_cube_dim; ++i)
            {
                int bb = 0;
                
                mover[i] = rand_num_uniform_Mersenne_twister(
                                                             1,
                                                             d_grid_points
                                                             );
                while(bb != 1 && m > 0)
                {
                    
                    mover[i] = rand_num_uniform_Mersenne_twister(
                                                                 1,
                                                                 d_grid_points
                                                                 );
                    for(int h = 0; h < m; ++h)
                    {
                        int j = mover[i] - movers[h][i];
                        
                        if(j == 0)
                        {
                            bb = 0;
                        }
                        
                        if(j != 0)
                        {
                            bb = 1;
                        }
                    }
                    //while
                }
                //i
            }
            //m
            movers.push_back(mover); // push a single coord back
        }
        
        all_movers.push_back(movers); //push a walker back
    }
    
    for(int w = 0; w < nwalkers; ++w)
    {
        for(int i = 0; i < nchasers; ++i)
        {
            int b = 1;
            all_movers[w][i].push_back(b); //identifier "1" indicates a chaser
        
            std::cout << all_movers[w][i][0] << "  " << all_movers[w][i][1] << "   " << all_movers[w][i][2] << "   " << all_movers[w][i][3] << '\n';
        }
        
        for(int i = (nchasers-1); i < total; ++i)
        {
            int p = 2;
            all_movers[w][i].push_back(p); //identifier "2" indicates a target
            
            std::cout << all_movers[w][i][0] << "  " << all_movers[w][i][1] << "   " << all_movers[w][i][2] << "   " << all_movers[w][i][3] << '\n';
        }
    }
    
    return std::make_tuple(
                           nwalkers,
                           all_movers
                           );
}


