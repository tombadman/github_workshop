#include <cmath>     //std::sqrt
#include <algorithm> //std::min_element
#include <iostream>
#include <geom.hpp>

int find_neighbor(
                  int idx,
                  int dim,
                  int grid_pts,
                  std::vector<std::vector<int>> movers
                  )
{
    // input the index of the current mover.
    // find it's nearest neighbor.
    
    // i think the best way (that immediately comes to mind)
    // is to sweep through the entire list of movers
    // once, and calculate rij.
    
    // if there is more than one nearest neighbor,
    // then return a vector of both of the indexes.
    
    // rijs for these two vectors will be added up
    // and treated as a 'net force'.
    
    std::vector<double> rij_mag;
    
    for(int i = 0; i < movers.size(); ++i)
    {
        if(movers[i][3] == movers[idx][3])
        {
            rij_mag.push_back(10000000000);
        }
        
        if(movers[i][3] != movers[idx][3])
        {
            auto harp  = min_img(
                                 movers[idx],
                                 movers[i],
                                 dim,
                                 grid_pts
                                 );
            
            int p = 0;
            
            for(int j = 0; j < harp.size(); ++j)
            {
                p += harp[j]*harp[j];
            }
            
            double q = std::sqrt(p);
            
            rij_mag.push_back(q);
        }
    }
    //int r = std::min_element(rij_mag.begin(),rij_mag.end());
    
    int idx_ = std::min_element(rij_mag.begin(),rij_mag.end()) - rij_mag.begin();
    
    std::cout << idx_ << '\n';
    
    return idx_;
}