#include <fstream>
#include <vector>
#include <iostream>
#include <string>
#include "cubes.hpp"
#include "jrandnum.hpp"

void output_chase_cubes(
                        int                                              d_cube_dim,
                        const int                                        d_grid_points,
                        const int                                        total,
                        const int                                        nsteps,
                        const std::vector<std::vector<std::vector<std::vector<int>>>> store_movers
                        )

{
    std::ofstream gnuplot_script;
    gnuplot_script.open("gps.sc", std::ios::out);
    gnuplot_script << "gnuplot -persist <<-EOFMarker " << '\n';
    
    int start = nsteps-1000;
    int b = 3;
    
    
    if(start < 0)
    {
        start = 1;
    }
    int W = 0;
    // output just one walker of data.
    
    
    for(int i = start; i < nsteps; ++i)
    {
        for(int p = 0; p < store_movers[i][W].size(); ++p)
        {
            std::string chaser_fn = "chaser.mover." + std::to_string(p) + ".step." + std::to_string(i) + ".dat";
            std::string target_fn = "target.mover." + std::to_string(p) + ".step." + std::to_string(i) + ".dat";
            
            if(store_movers[i][W][p][3] == 1) // chaser
            {
                std::ofstream ofs_c;
                
                ofs_c.open(chaser_fn, std::ios::out);
                
                auto cubes_chasers = cubes(
                                           b,
                                           store_movers[i][W][p]
                                           );
                
                for(int p = 0; p < 4; ++p)
                {
                    for(int q = 0; q < 5; ++q)
                    {
                        for(int r = 0; r < 3; ++r)
                        {
                            ofs_c << cubes_chasers[p][q][r] << "   ";
                        }
                        
                        ofs_c << "1";
                        
                        
                        ofs_c << '\n';
                    }
                    
                    ofs_c << '\n' ;
                }
                
                ofs_c << '\n';
                
                ofs_c.close();

            }
            
            if(store_movers[i][W][p][3] == 2) // target
            {
                std::ofstream ofs_t;
                
                ofs_t.open(target_fn, std::ios::out);
                
                auto cubes_targets = cubes(
                                           b, //cube side length
                                           store_movers[i][W][p]
                                           );
                
                for(int p = 0; p < 4; ++p)
                {
                    for(int q = 0; q < 5; ++q)
                    {
                        for(int r = 0; r < 3; ++r)
                        {
                            ofs_t <<   cubes_targets[p][q][r] << "   ";
                        }
                        
                        ofs_t << "1";
                        
                        
                        ofs_t << '\n';
                    }
                    
                    ofs_t << '\n' ;
                }
                
                ofs_t << '\n';
                
                ofs_t.close();
            }
            
            //from le cubes

        }
    }
    
    for(int i = start; i < nsteps; ++i)
    {
        std::string name = "config" + std::to_string(i) + ".png";
        gnuplot_script << "set terminal pngcairo font \"Arial,10\" size 1200,1200" << '\n';
        gnuplot_script << "set output \"" << name << "\" "<< '\n';
        gnuplot_script << "set xrange [-1:" << d_grid_points+b << "]" << '\n';
        gnuplot_script << "set yrange [-1:" << d_grid_points+b << "]" << '\n';
        gnuplot_script << "set zrange [-1:" << d_grid_points+b << "]" << '\n';
        gnuplot_script << "set cbrange [1:3]" << '\n';
        gnuplot_script << "set palette defined (\\" << '\n';
        gnuplot_script << "1 '#f8c932',\\" << '\n';
        gnuplot_script << "2 '#b5367a',\\" << '\n';
        gnuplot_script << "3 '#fec287',\\" << '\n';
        gnuplot_script << "4 '#fb8761',\\" << '\n';
        gnuplot_script << "5 '#a83655',\\" << '\n';
        gnuplot_script << "6 '#e35933',\\" << '\n';
        gnuplot_script << "7 '#88226a',\\" << '\n';
        gnuplot_script << "8 '#550f6d',\\" << '\n';
        gnuplot_script << "9 '#1f0c48',\\" << '\n';
        gnuplot_script << "10 '#000004')" << '\n';
        
        
        gnuplot_script << "set object 1 rectangle from screen 0,0 to screen 1,1 fillcolor rgb\"green\" behind" <<'\n';
        gnuplot_script << "set border 0" << '\n';
        gnuplot_script << "unset colorbox" << '\n';
        gnuplot_script << "set view 58,27,2" << '\n';
        gnuplot_script << "unset tics" << '\n';
        gnuplot_script << "set style line 1 lc rgb '#b90046' lt 1 lw 1" << '\n';
        gnuplot_script << "set style line 2 lc rgb '#b9b9b9' lt 1 lw 1" << '\n';
        gnuplot_script << "set style line 3 lc rgb '#2f4f4f' lt 1 lw 1" << '\n';
        gnuplot_script << "set style line 4 lc rgb '#00005d' lt 1 lw 1" << '\n';
        gnuplot_script << "set style line 6 lc rgb '#00005d' lt 1 lw 1" << '\n';
        gnuplot_script << "set style line 7 lc rgb '#00005d' lt 1 lw 1" << '\n';
        gnuplot_script << "set style line 8 lc rgb '#00005d' lt 1 lw 1" << '\n';
        gnuplot_script << "set style line 9 lc rgb '#00005d' lt 1 lw 1" << '\n';
        gnuplot_script << "set style line 10 lc rgb '#00005d' lt 1 lw 1" << '\n';
        gnuplot_script << "set pm3d depthorder hidden3d" << '\n';
        gnuplot_script << "set pm3d implicit" << '\n';
        gnuplot_script << "unset hidden3d"  << '\n';
        
        for(int p = 0; p < store_movers[i][W].size(); ++p)
        {
            std::string chaser_fn = "chaser.mover." + std::to_string(p) + ".step." + std::to_string(i) + ".dat";
            std::string target_fn = "target.mover." + std::to_string(p) + ".step." + std::to_string(i) + ".dat";
            
            if(p == 0)
            {
                gnuplot_script << "splot \"";
            }
            
            int c_line =  rand_num_uniform_Mersenne_twister(0,5);
            
            if(store_movers[i][W][p][d_cube_dim] == 1)
            {
               gnuplot_script << chaser_fn << "\" u 1:2:3:("<<c_line<<")  w l ls " << c_line;
            }
            
            
            
            int t_line =  rand_num_uniform_Mersenne_twister(6,10);
            if(store_movers[i][W][p][d_cube_dim] == 2)
            {
                gnuplot_script << target_fn << "\" u 1:2:3:("<<t_line<<")  w l ls " << t_line;
            }
            
            if(p != (store_movers[i][W].size()-1))
            {
             gnuplot_script << ", \"";
            }
            
        }
         gnuplot_script << '\n';
    }
    
    
    gnuplot_script << "EOFMarker" << '\n';
    
    gnuplot_script.close();
    
    std::ofstream animate_;
    animate_.open("animate.sc", std::ios::out);
    
    animate_ << "convert  -delay 7 config* -loop 1 animate.gif" << '\n';
    
    animate_.close();
    
}
