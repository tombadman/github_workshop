#ifndef CUBES_HPP
#define CUBES_HPP

#include <vector>

std::vector<std::vector<std::vector<int>>> cubes(
                                                 const int c,
                                                 const std::vector<int> vec
                                                 );

#endif