
#ifndef JRANDNUM_H	
#define JRANDNUM_H


// STL
#include <vector> // std::vector


//========================================================================
// NAME: void seed_Mersenne_twister()
// DESC: Seeds the Mersenne twiser PRNG.
//========================================================================
void seed_Mersenne_twister();

//========================================================================
// NAME: double rand_num_uniform_Mersenne_twister(double a, double b)
//       int rand_num_uniform_Mersenne_twister(int a, int b)
// DESC: Produces a (pseudo) random number, distributed uniformly on the interval [a, b].
//========================================================================
double rand_num_uniform_Mersenne_twister(double a, double b);
int rand_num_uniform_Mersenne_twister(int a, int b);

//========================================================================
// NAME: double rand_num_normal_Mersenne_twister(double mean, double stddev)
// DESC: Produces a (pseudo) random number, normally distributed.
//========================================================================
double rand_num_normal_Mersenne_twister(double mean, double stddev);

// TODO: the following should return the ints (not use an output argument)
//========================================================================
// NAME: void n_unique_rand_ints(int n, int min, int max, std::vector<int> &ints)
// DESC: Generates n random uniqe ints between min and max according to the Knuth--Fisher--Yates algorithm, storing the results in ints.
//========================================================================
void n_unique_rand_ints(int n, int min, int max, std::vector<int> &ints);

//#include "jrandnum/jrandnum.cpp"

#endif
