#ifndef GEOM_HPP
#define GEOM_HPP

#include <vector>
#include <tuple>


std::vector<int> min_img(
                         std::vector<int> ri,
                         std::vector<int> rj,
                         int              d_cube_dim,
                         int              d_grid_points
                         );


std::vector<int> wrap_coord(
                            const std::vector<int> coord,
                            const int              dim,
                            const int              d_grid_points
                            );

#endif